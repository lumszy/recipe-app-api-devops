variable "prefix" {
  default     = "raad"
  description = "prefix"
}

variable "project" {
  default     = "recipe-app-api-devops"
  description = "Project title"
}

variable "contact" {
  default     = "85thconsulting@gmail.com"
  description = "email address"
}

variable "db_username" {
  description = "Username for RDS postgress instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default     = "recipe-app-api-devops-bastion" #This key was created on AWS & must matches key name on AWS
  description = "key for EC2"
}

variable "ecr_image_api" {
  default     = "434657718982.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
  description = "ECR image for api"
}

variable "ecr_image_proxy" {
  default     = "434657718982.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
  description = "ECR image for proxy"
}


variable "django_secret_key" {
  default     = ""
  description = "Secret key for Django"
}




